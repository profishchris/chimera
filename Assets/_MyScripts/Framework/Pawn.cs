﻿using UnityEngine;
using System.Collections;

public class Pawn : MonoBehaviour
{    
    public float maxHealth;
    public float speed;
    public float currentSpeed;

    protected float health;

    public Transform gunBarrel;

    protected Animator anim;

    // Use this for initialization
    protected virtual void Start()
    {
        health = maxHealth;
        currentSpeed = speed;

        anim = GetComponent<Animator>();
    }

    protected virtual void Update() 
    {
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    protected virtual void FixedUpdate() 
    {
        // reset speed
        currentSpeed = speed;
    }

    public virtual void ModifyHealth(float mod)
    {
        health += mod;
        health = Mathf.Clamp(health, 0.0f, maxHealth);
    }

    public virtual float GetHealth()
    {
        return health;
    }
}
