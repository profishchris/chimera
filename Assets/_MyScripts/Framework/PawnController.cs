﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PawnController : MonoBehaviour
{
    public string Name          { get; set; }
    public string Description   { get; set; }
    public PlayerPawn GamePawn  { get; set; }

    public virtual void Start()
    {
        GamePawn = (PlayerPawn)transform.GetComponent<Pawn>();

        Name        = "NoName";
        Description = "A Bot";
    }

    public virtual void Update() { }
    public virtual void FixedUpdate() { }



    public void TeleportTo(Vector3 destination)
    {

    }

    protected List<PawnType> GetOtherPawnsInRange<PawnType>(float range = 50, bool requireLineOfSight = true) where PawnType : Pawn
    {
        List<PawnType> pawnList = new List<PawnType>();

        // Get all pawns of Type
        PawnType[] allPawns = FindObjectsOfType<PawnType>();

        // Cache pawns position
        Vector3 myPosition = GamePawn.gunBarrel.position; // GamePawn.transform.position;
        Vector3 directionToTarget;
        foreach (PawnType candidate in allPawns)
        {
            // Ignore self
            if (candidate == GamePawn) continue;

            // Check distance
            directionToTarget = candidate.transform.position - myPosition;
            if (directionToTarget.magnitude <= range)
            {
                if (requireLineOfSight == false)
                {
                    // No LoS check, add to list
                    pawnList.Add(candidate);
                }
                else
                {
                    // Check LoS
                    RaycastHit hit;
                    if (Physics.Raycast(myPosition, directionToTarget, out hit, range, GameController.EnviromentMask))
                    {

                    }
                    else
                    {

                        pawnList.Add(candidate);

                        //// If there are no obstructions the ray will hit the candidate
                        //PawnType rayTarget = hit.collider.GetComponent<PawnType>();
                        //if (rayTarget
                        // && rayTarget.Equals(candidate))
                        //{
                        //    pawnList.Add(candidate);
                        //    Debug.DrawLine(myPosition, hit.point, Color.green);
                        //}
                        //else
                        //{
                        //    Debug.DrawLine(myPosition, hit.point, Color.cyan);
                        //}
                    }
                    //else
                    //{
                    //    Debug.DrawLine(myPosition, myPosition + (directionToTarget * range), Color.red);
                    //}
                }
            }
        }


        return pawnList;
    }

    protected PawnType GetNearestVisiblePawn<PawnType>(float range = 50, bool requireLineOfSight = true) where PawnType : Pawn
    {
        // Get visible PawnTypes in range
        List<PawnType> visiblePawns = GetOtherPawnsInRange<PawnType>(range, requireLineOfSight);

        // Calculate the nearest PawnType
        int index = -1;
        Vector3 myPosition = GamePawn.transform.position;
        float distance = 0.0f;
        float shortestDistance = range + 1.0f;
        for (int i = 0; i < visiblePawns.Count; ++i)
        {
            distance = (visiblePawns[i].transform.position - myPosition).magnitude;
            if (distance < shortestDistance)
            {
                shortestDistance = distance;
                index = i;
            }
        }

        if (index < 0) return null;
        return visiblePawns[index];
    }
    
    protected Vector3 GetLocationInRange(float min, float max, Vector3 pos)
    {
        return new Vector3(pos.x + Random.Range(min, max),
                            1.0f,
                            pos.z + Random.Range(min, max));
    }
}
