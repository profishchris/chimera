﻿using UnityEngine;
using System.Collections;
using ContextualSteeringBehaviour;

public abstract class AIPawn : PawnController
{
    private const int c_Fidelity                = 32;
    protected int m_UpdateCount                 = 50;

    protected ContextMap m_ContextMap           = new ContextMap(c_Fidelity);
    protected Vector3 m_TargetDestination       = Vector3.zero;
    protected Vector3 m_PositivedLocation       = Vector3.zero;
    protected Pawn m_Target                     = null;

    protected bool m_InputDetected              = true;
    protected bool m_ShowDebug                  = true;


    protected abstract void GenerateContext(bool resetContext = true);
    protected abstract void ApplyContext();

    protected virtual void DraawDebug()
    {
        m_ContextMap.DrawDebug(transform.position);

        // Draw the current Positived destination
        Debug.DrawLine(transform.position, m_PositivedLocation, Color.yellow);
        Debug.DrawLine(transform.position, m_TargetDestination, Color.magenta);
    }

    protected virtual bool NewDestinationIsRequired()
    {
        m_UpdateCount++;
        if (m_UpdateCount > 6)
        {
            m_UpdateCount = 0;
            return true;
        }

        return false;
    }

    /// <summary>
    /// Set a destination projected in the given direction
    /// </summary>
    /// <param name="direction">direction to move in</param>
    /// <param name="speedRatio">percentage of speed at which to project destination</param>
    protected void SetDestination(Vector3 direction, float speedRatio = 1.0f)
    {
        if (GamePawn.Agent.pathPending == false)
        {
            m_PositivedLocation = transform.position + (direction * GamePawn.speed * speedRatio);
            GamePawn.Agent.SetDestination(m_PositivedLocation);
        }

    }

    /// <summary>
    /// Set a destination projected in the given direction
    /// </summary>
    /// <param name="direction">direction to move in</param>
    /// <param name="speedRatio">percentage of speed at which to project destination</param>
    protected void MoveTowardDestination(Vector3 direction, float speedRatio = 1.0f)
    {
        m_PositivedLocation    = (direction * GamePawn.speed * speedRatio);
        GamePawn.Agent.speed = GamePawn.speed * speedRatio;
        GamePawn.Agent.Move(direction * speedRatio);
    }



    public override void Update()
    {
        base.Update();
        
        if (NewDestinationIsRequired())
        {
            GenerateContext();

            ApplyContext();
        }


        if (m_ShowDebug)
        {
            DraawDebug();
        }
    }
}
