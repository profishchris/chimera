﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ContextualSteeringBehaviour
{
    public class ContextMap
    {
        public ContextMap(int fidelity)
        {
            Fidelity = fidelity;
            GenerateDirections(Fidelity);

            Negative = new float[Fidelity];
            Positive = new float[Fidelity];

            ResetContext();
        }


        public int Fidelity { get; set; }
        public Vector3[] Directions { get; set; }

        public float[] Negative { get; set; }
        public float[] Positive { get; set; }


        /// <summary>
        /// Set contexts back to zero
        /// </summary>
        public void ResetContext()
        {
            for (int i = 0; i < Fidelity; i++)
            {
                Positive[i] = -1.0f;
                Negative[i] = -1.0f;
            }
        }

        public void ResetPositiveContext()
        {
            for (int i = 0; i < Fidelity; i++)
            {
                Positive[i] = -1.0f;
            }
        }

        public void ResetNegativeContext()
        {
            for (int i = 0; i < Fidelity; i++)
            {
                Negative[i] = -1.0f;
            }
        }


        public void DrawDebug(Vector3 origin)
        {
            // Draw the current context
            for (int i = 0; i < Fidelity; i++)
            {
                if (Negative[i] > 0.0f)
                {
                    Debug.DrawRay(origin, Directions[i] * Negative[i], Color.red);
                }

                if (Positive[i] > 0.0f)
                {
                    Debug.DrawRay(origin, Directions[i] * Positive[i], Color.green);
                }
            }
        }



        /// <summary>
        /// Generates equally spaced directions, rotated around a single point
        /// </summary>
        /// <param name="fidelity">number of directions to generate</param>
        private void GenerateDirections(int fidelity)
        {
            Directions = new Vector3[fidelity];
            Quaternion eularTransform;

            float epsilon = 360 / fidelity;
            for (int i = 0; i < fidelity; i++)
            {
                eularTransform = Quaternion.AngleAxis(epsilon * i, Vector3.up);
                Directions[i] = eularTransform * Vector3.forward;
            }
        }
    }
}
