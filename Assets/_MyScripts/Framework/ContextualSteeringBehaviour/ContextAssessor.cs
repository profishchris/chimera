﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ContextualSteeringBehaviour
{
    public static class ContextAssessor
    {
        /// <summary>
        /// Updates the array (if higher than current) based on the angle TO the target
        /// </summary>
        /// <param name="contextArray">Context to update<</param>
        /// <param name="startPosition">Position to evaluate from</param>
        /// <param name="targetPosition">Object to steer towards</param>
        /// <param name="modifier">Modifier to apply</param>
        public static void SteerTowards(ContextMap contextMap, Vector3 startPosition, Vector3 targetPosition, float modifier = 1.0f)
        {
            // Calculate the direction towards the target
            Vector3 directionToTarget = targetPosition - startPosition;
            directionToTarget.Normalize();

            float currentContext;
            for (int i = 0; i < contextMap.Fidelity; i++)
            {
                // Context is based on dot product
                currentContext = Vector3.Dot(directionToTarget, contextMap.Directions[i]) * modifier;

                // Only positive context
                if (currentContext > 0.0f && currentContext > contextMap.Positive[i])
                {
                    // Update context if higher than existing
                    contextMap.Positive[i] = currentContext;
                }
            }
        }

        /// <summary>
        ///  Updates the context (if higher than current) based on angle AWAY from the target
        /// </summary>
        /// <param name="contextArray">Context to update<</param>
        /// <param name="startPosition">Position to evaluate from</param>
        /// <param name="targetPosition">Object to steer from</param>
        /// <param name="modifier">Modifier to apply</param>
        public static void SteerAway(ContextMap contextMap, Vector3 startPosition, Vector3 targetPosition, float modifier = 1.0f)
        {
            // Calculate the direction towards the target
            Vector3 directionToTarget = targetPosition - startPosition;
            directionToTarget.Normalize();

            float currentContext = -1.0f;
            for (int i = 0; i < contextMap.Fidelity; i++)
            {
                // Context is based on dot product
                currentContext = Vector3.Dot(directionToTarget, contextMap.Directions[i]) * modifier;

                // Only positive contexts              
                if (currentContext > 0.0f && currentContext > contextMap.Positive[i])
                {
                    // Update context if higher than existing
                    contextMap.Positive[i] = currentContext;
                }
            }
        }

        /// <summary>
        /// Update contexts based on blocking objects
        /// </summary>
        public static void AvoidObstacles(ContextMap contextMap, Vector3 startPosition, float distance)
        {
            for (int i = 0; i < contextMap.Fidelity; i++)
            {
                RaycastHit hit;
                if (Physics.Raycast(startPosition, contextMap.Directions[i], out hit, distance, GameController.EnviromentMask))
                {
                    float danger = 1.0f - CalculateNormalizedDistance(startPosition, hit.point, 0.0f, distance);
                    if (danger > contextMap.Negative[i])
                    {
                        contextMap.Negative[i] = danger;
                    }
                }
            }
        }

        public static float CalculateNormalizedDistance(Vector3 origin, Vector3 destination, float minDistance, float maxDistance)
        {
            float distance = Vector3.Distance(origin, destination);

            // Normalize the distance and clamp
            distance = (distance - minDistance) / (maxDistance - minDistance);
            return Mathf.Clamp(distance, -1.0f, 1.0f);
        }
    }
}
