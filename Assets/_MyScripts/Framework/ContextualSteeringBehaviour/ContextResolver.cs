﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ContextualSteeringBehaviour
{
    public static class ContextResolver
    {
        /// <summary>
        /// Find the most desired slot from a list of safe slots
        /// </summary>
        /// <returns></returns>
        public static int FindSafestDesiredSlot(ContextMap contextMap, float dangerThreshold)
        {
            int mostDesiredSlot = -1;

            // Find the most desired direction from the least dangerous slots
            float highestDesire = 0.1f;
            for (int i = 0; i < contextMap.Fidelity; i++)
            {
                // Select safe, desirable slots
                if (contextMap.Negative[i] < dangerThreshold
                 && contextMap.Negative[i] >= 0.0f)
                {
                    // Remember the most desirable
                    if (contextMap.Positive[i] > highestDesire)
                    {
                        highestDesire = contextMap.Positive[i];
                        mostDesiredSlot = i;
                    }
                }
            }

            return mostDesiredSlot;
        }

        /// <summary>
        /// Find the overall most desirable slot
        /// </summary>
        /// <returns></returns>
        public static int FindCombinedSafestSlot(ContextMap contextMap)
        {
            float leastDangerous = -1.0f;
            int leastDangerousSlot = -1;

            float[] combined = new float[contextMap.Fidelity];
            for (int i = 0; i < contextMap.Fidelity; i++)
            {
                if (contextMap.Negative[i] < 1.0f)
                {
                    combined[i] = contextMap.Positive[i] - contextMap.Negative[i];
                }
                else
                {
                    combined[i] = -50.0f; // Do NOT go this way
                }

                if (combined[i] > leastDangerous)
                {
                    leastDangerous = combined[i];
                    leastDangerousSlot = i;
                }

            }

            return leastDangerousSlot;
        }
    }
}
