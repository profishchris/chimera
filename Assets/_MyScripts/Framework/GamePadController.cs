﻿using UnityEngine;
using System.Collections;

public class GamePadController : PawnController 
{

    static float deadZone = 0.1f;

	public override void Update() 
    {
        if (InputManager.GetAxisAsButton("Fire1"))
        {
            GamePawn.CharacterClass.UseStandardMove(GamePawn.transform, GamePawn.gunBarrel);
        }

        if (InputManager.GetAxisAsButton("Fire2"))
        {
            GamePawn.CharacterClass.UseSpecialMove(GamePawn.transform, GamePawn.gunBarrel);
        }
        else
        {
            GamePawn.CharacterClass.AllowSpecialFire();
        }

        // InputManager.TestAxis("MoveX", "", "MoveY", deadZone);
	}

    public override void FixedUpdate()
    {
        if (!GamePawn) return;

        // Move and animate
        float xVelocity  = InputManager.GetAxisRaw("MoveX", 1);
        float zVelocity  = InputManager.GetAxisRaw("MoveY", 1);
        Vector3 velocity = InputManager.ApplyDeadzone(deadZone, xVelocity, 0.0f, zVelocity);
        velocity         = Vector3.ClampMagnitude(velocity, 1.0f);
        if (velocity != Vector3.zero)
        {
            GamePawn.GetComponent<Rigidbody>().velocity  = velocity * GamePawn.currentSpeed;
            GamePawn.AnimSpeed = velocity.magnitude * GamePawn.currentSpeed;
        }
        else
        {
            GamePawn.GetComponent<Rigidbody>().velocity = Vector3.zero;
            GamePawn.AnimSpeed = 0.0f;
        }

        // Turn or face velocity         
        float xDirection  = InputManager.GetAxisRaw("LookX");
        float zDirection  = InputManager.GetAxisRaw("LookY");
        Vector3 direction = InputManager.ApplyDeadzone(0.2f, xDirection, 0.0f, zDirection);
        if (direction != Vector3.zero)
        {
            GamePawn.transform.LookAt(GamePawn.transform.position + direction, Vector3.up);
        }
        else if (velocity != Vector3.zero)
        {
            GamePawn.transform.LookAt(GamePawn.transform.position + velocity, Vector3.up);
        }
    }
}
