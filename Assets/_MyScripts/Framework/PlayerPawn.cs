using UnityEngine;
using System.Collections;


public class PlayerPawn: Pawn 
{	
    public PawnController   PawnController  { get; set; }
    public PlayerClass      CharacterClass  { get; set; }
    public NavMeshAgent     Agent           { get; set; }

    public float SpecialAmmmoCount  { get { return CharacterClass.GetSpecialCount(); } }
    public Color LightColor         { get; set; }
    public float AnimSpeed          { set { anim.SetFloat("Speed", value); } }

	// Use this for initialization
    protected override void Start()
    {
        base.Start();

        // Create indicator on pawn
        m_Indicator                    = new PlayerIndicator(LightColor);
        m_Indicator.transform.position = new Vector3(0, 4, 0) + transform.position;
        m_Indicator.transform.rotation = Quaternion.AngleAxis(90, Vector3.right);
        m_Indicator.transform.parent   = gameObject.transform;

        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
	
	// Update is called once per frame
	protected override void Update () 
	{
        if (health > 0)
        {
            PawnController.Update();
            CharacterClass.Update();

            base.Update();
        }

	}

    // Time step
    protected override void FixedUpdate()
    {
        PawnController.FixedUpdate();
        base.FixedUpdate();
    }

    // Set input type and Character class
    public void SetComponents<ControllerType, JobType>() 
        where ControllerType : PawnController
        where JobType : PlayerClass
    {
        PawnController = gameObject.AddComponent<ControllerType>();
        CharacterClass = gameObject.AddComponent<JobType>();
        Agent          = GetComponent<NavMeshAgent>();

        health = maxHealth;
    }

    public void SetHpToMax()
    {
        health = maxHealth;
    }

    public void TeleportTo(Vector3 destination)
    {
        // Disable agent to prevent the pawn from 'snapping' back
        Agent.enabled = false;
        transform.position = destination;
        Agent.enabled = true;
    }



    private PlayerIndicator m_Indicator = null;
    
}
