﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputManager
{
    // Properties
    private static Dictionary<string, string> m_KeyMap = new Dictionary<string, string>();
    public  static Dictionary<string, string> KeyMap
    {
        get
        {
            if(m_KeyMap.Count < 1)
            {
                LoadKeyMap();
            }

            return m_KeyMap;
        }
    }

    private static string m_InputMask = "Pad{0}_{1}";
    public  static string InputMask
    {
        get { return m_InputMask; }
        set { m_InputMask = value; }
    }


    // Constructor (private)
    private InputManager() { }
    

    // Key map functions
    private static void LoadKeyMap()
    {
        // Load from file else fall back to defaults
        if(LoadKeyMapFromFile() == false)
        {
            LoadDefaultKeyMap();
        }
    }

    private static void LoadDefaultKeyMap()
    {
        m_KeyMap.Clear();

        m_KeyMap.Add("MoveX", "LeftX");
        m_KeyMap.Add("MoveY", "LeftY");

        m_KeyMap.Add("LookX", "RightX");
        m_KeyMap.Add("LookY", "RightY");

        m_KeyMap.Add("Fire1", "R2");
        m_KeyMap.Add("Fire2", "L2");

        m_KeyMap.Add("1",  "R1");
        m_KeyMap.Add("2",  "L1");
        m_KeyMap.Add("3",  "X");
        m_KeyMap.Add("4",  "Y");
        m_KeyMap.Add("5",  "A");
        m_KeyMap.Add("6",  "B");
        m_KeyMap.Add("7",  "Back");
        m_KeyMap.Add("8",  "Start");
        m_KeyMap.Add("9",  "Horizontal");
        m_KeyMap.Add("10", "Vertical");
        m_KeyMap.Add("11", "R3");
        m_KeyMap.Add("12", "L3");
    }

    private static bool LoadKeyMapFromFile()
    {
        // TODO: Load from file
        return false;
    }


    // Wrapper functions
    public static float GetAxis(string axis, int padNum = 1)
    {
        float axisValue   = 0.0f;
        string mappedAxis = GetMappedName(axis, padNum);

        if (string.IsNullOrEmpty(mappedAxis) == false)
        {
            axisValue =  Input.GetAxis( mappedAxis );
        }

        return axisValue;
    }

    public static float GetAxisRaw(string axis, int padNum = 1)
    {
        float axisValue   = 0.0f;
        string mappedAxis = GetMappedName(axis, padNum);

        if (string.IsNullOrEmpty(mappedAxis) == false)
        {
            axisValue = Input.GetAxisRaw( mappedAxis );
        }

        return axisValue;
    }

    public static bool  GetButton(string button, int padNum = 1)
    {
        bool isButtonPressed = false;
        string mappedButton  = GetMappedName(button, padNum);

        if (string.IsNullOrEmpty(mappedButton) == false)
        {
            isButtonPressed = Input.GetButton(GetMappedName(button, padNum));
        }

        return isButtonPressed;
    }

    public static bool  GetButtonUp(string button, int padNum = 1)
    {
        bool isButtonUp     = false;
        string mappedButton = GetMappedName(button, padNum);

        if (string.IsNullOrEmpty(mappedButton) == false)
        {
            isButtonUp = Input.GetButtonUp(GetMappedName(button, padNum));
        }

        return isButtonUp;
    }

    public static bool  GetButtonDown(string button, int padNum = 1)
    {
        bool isButtonDown   = false;
        string mappedButton = GetMappedName(button, padNum);

        if(string.IsNullOrEmpty(mappedButton) == false)
        {
            isButtonDown = Input.GetButtonDown(GetMappedName(button, padNum));
        }

        return isButtonDown;
    }

    public static bool  GetAxisAsButtonUp(string button, int padNum = 1)
    {
        bool isAxisUp = false;
        string mappedButton = GetMappedName(button, padNum);

        if(string.IsNullOrEmpty(mappedButton) == false)
        {
            isAxisUp = GetAxis(button, padNum) == 0;
        }

        return isAxisUp;
    }

    public static bool  GetAxisAsButtonDown(string button, int padNum = 1)
    {
        bool isAxisDown = false;
        string mappedButton = GetMappedName(button, padNum);

        if (string.IsNullOrEmpty(mappedButton) == false)
        {
            isAxisDown = GetAxis(button, padNum) != 0;
        }

        return isAxisDown;
    }

    public static bool  GetAxisAsButton(string button, int padNum = 1, float deadZone = 0.25f)
    {
        bool isAxisHeld = false;
        string mappedButton = GetMappedName(button, padNum);

        if (string.IsNullOrEmpty(mappedButton) == false)
        {
            isAxisHeld = ApplyAbsDeadzone(deadZone, GetAxis(button, padNum)) != 0;
        }

        return isAxisHeld;
    }


    // Helper functions
    private static string GetMappedName(string name, int padNum)
    {
        if (KeyMap.ContainsKey(name))
        {
            return string.Format(m_InputMask, padNum, KeyMap[name]);
        }
        else
        {
            Debug.LogWarning(string.Format("[InputManager] Button/Axis not found for pad {0}: {1}", padNum, name));
        }

        return string.Empty;
    }

    public static float   ApplyDeadzone(float deadZone, float value)
    {
        float normalizedValue = 0.0f;

        if (value >= deadZone)
        {
            normalizedValue = value * ((value - deadZone) / (1 - deadZone));
        }

        return normalizedValue;
    }

    public static Vector3 ApplyDeadzone(float deadZone, Vector3 axisValue)
    {
        return ApplyDeadzone(deadZone, axisValue.x, axisValue.y, axisValue.z);
    }

    public static Vector3 ApplyDeadzone(float deadZone, float xAxis, float yAxis, float zAxis)
    {
        Vector3 scaledRadialDeadzone = Vector3.zero;
        Vector3 axisValue            = new Vector3(xAxis, yAxis, zAxis);

        if(axisValue.magnitude >= deadZone)
        {
            // Normalize the vector based on the deadZone
            scaledRadialDeadzone = axisValue.normalized * ((axisValue.magnitude - deadZone) / (1 - deadZone));
        }

        return scaledRadialDeadzone;
    }  

    public static float   ApplyAbsDeadzone(float absDeadZone, float value)
    {
        value = Mathf.Abs(value);
        float normalizedValue = 0.0f;

        if (value >= absDeadZone)
        {
            normalizedValue = value * ((value - absDeadZone) / (1 - absDeadZone));
        }

        return normalizedValue;
    }


    // Debug functions
    public static void    TestMappedInput(int padNum = 1, float absDeadZone = 0.25f)
    {
        foreach (KeyValuePair<string, string> item in m_KeyMap)
        {
            string axisString = string.Format(m_InputMask, padNum, item.Value);
            float axisValue   = ApplyAbsDeadzone(absDeadZone, Input.GetAxisRaw(axisString));

            if (axisValue != 0)
            {
                Debug.Log(string.Format("{0} = {1}", GetMappedName(item.Value, padNum), axisValue));
            }
        }
    }

    public static float   TestInput(string name, int padNum = 1, float absDeadZone = 0.25f)
    {
        float value = ApplyAbsDeadzone(absDeadZone, GetAxisRaw(name));

        if(value != 0.0f)
        {
            Debug.Log(string.Format("{0} = {1}", GetMappedName(name, padNum), value));
        }

        return value;
    }

    public static Vector3 TestAxis(string xAxis, string yAxis, string zAxis, float deadZone = 0.25f)
    {
        Vector3 axis = ApplyDeadzone(deadZone, GetAxisRaw(xAxis), GetAxisRaw(yAxis), GetAxisRaw(zAxis));

        if (axis != Vector3.zero)
        {
            Debug.Log(string.Format("({0}, {1}, {2}) = ({3}, {4}, {5})", xAxis, yAxis, zAxis, axis.x, axis.y, axis.z));
        }

        return axis;
    }
}