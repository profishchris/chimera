﻿using UnityEngine;
using System.Collections;

public class PlayerIndicator
{
    public PlayerIndicator(Color c)
    {
        // Red default
        c = (c == Color.clear ? Color.red : c);


        // Create light
        m_LightObject              = new GameObject();
        m_LightObject.name         = "PlayerIndicator";

        Light lightComponent     = m_LightObject.AddComponent<Light>() as Light;
        lightComponent.color     = c;
        lightComponent.type      = LightType.Spot;
        lightComponent.range     = 10;
        lightComponent.spotAngle = 30;
        lightComponent.intensity = 5.0f;
        lightComponent.enabled   = true;
    }

    public Transform transform
    {
        get { return m_LightObject.transform; }
    }



    private GameObject m_LightObject = null;
}