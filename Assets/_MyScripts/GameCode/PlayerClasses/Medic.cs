﻿using UnityEngine;
using System.Collections;

public class Medic : PlayerClass 
{

	// Use this for initialization
	void OnEnable() 
    {
        standardMove = (GameObject)Resources.Load("Laser_HP");
        standardFireRate = 0.4f;

        specialMove = (GameObject)Resources.Load("Laser_Area");
        specialChargeRate = 12.0f;
	}
}
