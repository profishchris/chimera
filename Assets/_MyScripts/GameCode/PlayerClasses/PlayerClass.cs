﻿using UnityEngine;
using System.Collections;

public class PlayerClass : MonoBehaviour 
{
    const int maxSpecialCount = 3;

    protected GameObject standardMove   = null;
    protected float standardFireRate    = 0.0f;
    protected float standardTimer       = 0.0f;

    protected GameObject specialMove    = null;
    protected float specialChargeRate   = 7.5f;
    protected float specialTimer        = 0.0f;
    protected int specialCount          = 1;
    protected bool canFireSpecial       = true;

	// Use this for initialization
	void Start()
    {
        UpdateFireTime();
        specialTimer = Time.time + (specialChargeRate * 2.0f);
        Debug.Log("YO");
	}

    public void Update()
    {
        // Award special move
        if (Time.time > specialTimer 
        && specialCount < maxSpecialCount)
        {   
            specialCount++;
            UpdateSpecialTimer();
        }
    }


    public virtual bool UseStandardMove(Transform player, Transform origin)
    {
        if (Time.time > standardTimer)
        {
            AllowSpecialFire();

            UpdateFireTime();
            GameObject shot = (GameObject)Instantiate(standardMove, origin.position, player.rotation);

            // Play the shot's audio clip
            AudioSource.PlayClipAtPoint(shot.GetComponent<AudioSource>().clip, player.position);
            return true;
        }

        return false;
    }

    public virtual bool UseSpecialMove(Transform player, Transform origin)
    {
        if (CanUseSpecialMove())
        {
            canFireSpecial = false;
            specialCount--;

            Instantiate(specialMove, origin.position, player.rotation);
            return true;
        }

        return false;
    }

    public bool CanUseSpecialMove()
    {
        return specialCount > 0 && canFireSpecial;
    }

    public void AllowSpecialFire()
    {
        canFireSpecial = true;
    }

    public int GetSpecialCount()
    {
        return specialCount;
    }

    protected void UpdateFireTime()
    {
        standardTimer = Time.time + standardFireRate;
    }

    protected void UpdateSpecialTimer()
    {
        specialTimer = Time.time + specialChargeRate;
    }
}
