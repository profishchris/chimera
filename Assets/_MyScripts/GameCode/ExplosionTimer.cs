﻿using UnityEngine;
using System.Collections;

public class ExplosionTimer : MonoBehaviour 
{
    public float deathTimer = 1.0f;

	// Use this for initialization
	void Start () 
    {
        deathTimer += Time.time;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.time > deathTimer)
        {
            Destroy(this.gameObject);
        }
	}
}
