﻿using UnityEngine;
using System.Collections;

public class Enemy : Pawn 
{
    private NavMeshAgent agent;
    public PlayerPawn target;

	// Use this for initialization
	protected override void Start () 
    {
        base.Start();
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	protected override void Update ()
    {
        // Use current speed
        anim.speed = currentSpeed;

        base.Update();

        if (health < 1)
        {
            GetComponent<AudioSource>().Play();
            Destroy(this.gameObject);
        }
        else if (target)
        {
            float distanceToTarget = (target.transform.position - transform.position).magnitude;
            if (distanceToTarget < 2.0f)
            {
                transform.LookAt(target.transform.position);
                target.GetComponent<Pawn>().ModifyHealth(-0.025f);
                agent.Stop(true);
                agent.velocity = Vector3.zero;
            }
            else
            {
                agent.SetDestination(target.transform.position);
            }

            if (agent.velocity != Vector3.zero)
            {
                anim.SetFloat("Speed", agent.speed);
            }
            else
            {
                anim.SetFloat("Speed", 0);
            }
        }
        else
        {
            
        }
	}
}
