﻿using UnityEngine;
using System.Collections;

public class PlayerCam : MonoBehaviour 
{
    public float m_CamHeight = 25.0f;

    public void Start()
    {
        Pawn p         = GameController.Instance().Player;
        m_FollowTarget = p.transform;
    }
	

	void Update () 
    {
        if (m_FollowTarget)
        {
            Vector3 newPos     = m_FollowTarget.position;
            newPos.y          += m_CamHeight;
            transform.position = newPos;
        }
	}


    private Transform m_FollowTarget = null;
}
