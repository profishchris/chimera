﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RaiseWalls : MonoBehaviour 
{
    public float m_StartHeight  = -10.0f;
    public float m_FinishHeight = 2.0f;

    public float m_Time         = 0.5f;


	// Use this for initialization
	public void Start () 
    {
        for(int i = 0; i < transform.childCount; ++i)
        {
            Transform child = transform.GetChild(i);
            m_Children.Add(child);

            Vector3 pos    = child.position;
            pos.y          = m_StartHeight;
            child.position = pos;
        }

        m_Timer = m_Time;
	}
	
	// Update is called once per frame
	public void Update () 
    {
        if(m_Timer > 0.0f)
        {
            m_Timer    -= Time.deltaTime;
            float delta = 1.0f - (m_Timer / m_Time);
            float y     = Mathf.Lerp(m_StartHeight, m_FinishHeight, delta);

            for (int i = 0; i < m_Children.Count; ++i)
            {
                Transform child = m_Children[i];

                Vector3 pos    = child.position;
                pos.y          = y;
                child.position = pos;
            }
        }	
	}


    private List<Transform> m_Children = new List<Transform>();
    private float m_Timer              = 0.0f;
}
