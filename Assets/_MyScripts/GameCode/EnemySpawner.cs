﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour 
{
    public  float m_SpawnRate = 15.0f;
    private float m_SpawnTime = 0.0f;

    public GameObject m_Creature;
    private List<Transform> m_Spawners = new List<Transform>();
    private EnemySquadController m_Manager;
    private bool m_NeedEnemy = true;

	// Use this for initialization
	void Start () 
    {
        m_SpawnTime = Time.time;

        // Get refrences to all spawners
        foreach (Transform child in transform)
        {
            m_Spawners.Add(child);
        }

        m_Manager = GetComponent<EnemySquadController>();
    }
	
	// Update is called once per frame
	void Update () 
    {
        if (m_NeedEnemy)
        {
            for (int i = 0; i < 2; i++)
            {
                Transform spawner = m_Spawners[i];
                GameObject newRecruit = (GameObject)Instantiate(m_Creature, spawner.position, spawner.rotation);
                m_Manager.AddSquadMember(newRecruit.GetComponent<Enemy>());
            }

            m_NeedEnemy = false;
        }

        if (Time.time > m_SpawnTime)
        {
            // Pick a random spwaner
            Transform spawner = m_Spawners[Random.Range(0, m_Spawners.Count)];

            // Create Guard
            if (m_Creature != null)
            {
                GameObject newRecruit = (GameObject)Instantiate(m_Creature, spawner.position, spawner.rotation);
                m_Manager.AddSquadMember(newRecruit.GetComponent<Enemy>());
            }
            else
            {
                Debug.LogError("No creature to spawn");
            }

            // Update timer
            m_SpawnTime = Time.time + 1.0f;
        }
	}

    void FixedUpdate()
    {
        m_SpawnRate -= 0.0001f;
    }

    public GameObject GetPrefab()
    {
        return m_Creature;
    }
}
