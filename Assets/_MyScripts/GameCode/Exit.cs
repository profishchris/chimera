﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour 
{
    public void OnTriggerEnter(Collider other)
    {
        if(other.GetComponent<PlayerPawn>() != null)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
