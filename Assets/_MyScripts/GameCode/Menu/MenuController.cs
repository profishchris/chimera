﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MenuController : MonoBehaviour 
{
    public List<string> buttonChoices = new List<string>();
    
	// Use this for initialization
	void Start () 
    {
        buttonChoices.Capacity = 4;
        for(int i = 0; i < 4; i++)
        {
            buttonChoices.Add("Soldier");
        }
	}
	
	// Update is called once per frame
	void Update () 
    {
	
	}


    void OnGUI()
    {
        // Buttons and labels, one for each corner
        GUI.Box(new Rect(0, 0, 100, 50), "Human");
        if(GUI.Button(new Rect(10, 25, 80, 20), buttonChoices[0])) UpdateButton(0);

        GUI.Box(new Rect(Screen.width - 100, 0, 100, 50), "Veteran");
        if (GUI.Button(new Rect(Screen.width - 90, 25, 80, 20), buttonChoices[1])) UpdateButton(1);

        GUI.Box(new Rect(Screen.width - 100, Screen.height - 50, 100, 50), "Grunt");
        if (GUI.Button(new Rect(Screen.width - 90, Screen.height - 25, 80, 20), buttonChoices[2])) UpdateButton(2);

        GUI.Box(new Rect(0, Screen.height - 50, 100, 50), "Rookie");
        if (GUI.Button(new Rect(10, Screen.height - 25, 80, 20), buttonChoices[3])) UpdateButton(3);


        // Confirmation
        float centerX = Screen.width * 0.5f;
        float centerY = Screen.height * 0.5f;
        Rect offsetRectangle = new Rect(centerX - 80, centerY - 40, 160, 80);
        if (GUI.Button(offsetRectangle, "Start Game")) StartGame();
    }

    void StartGame()
    {
        Object.DontDestroyOnLoad(FindObjectOfType<MenuController>());
        Application.LoadLevel(2);
    }

    void UpdateButton(int index)
    {
        if (buttonChoices[index] == "Soldier")
        {
            buttonChoices[index] = "Medic";
        }
        else
        {
            buttonChoices[index] = "Soldier";
        }
    }
}
