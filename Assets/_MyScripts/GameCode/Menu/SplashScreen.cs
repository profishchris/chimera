﻿using UnityEngine;
using System.Collections;

public class SplashScreen : MonoBehaviour 
{
    float waitTime = 5.0f;

    void OnStart()
    {
        waitTime = Time.time + waitTime;
    }


    void Update()
    {
        if (Time.time > waitTime)
        {
            Application.LoadLevel(1);
        }
    }
}
