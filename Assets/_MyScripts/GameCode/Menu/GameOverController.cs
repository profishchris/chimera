﻿using UnityEngine;
using System.Collections;

public class GameOverController : MonoBehaviour 
{
    private float score = 100.0f;

	// Use this for initialization
	void Start () 
    {
        DisplayPlayerStats stats = FindObjectOfType<DisplayPlayerStats>();
        if (stats)
        {
            score = stats.currentScore;
            Destroy(stats);
        }
        else
        {
            Debug.Log("Stats not found");
        }
	}
	

	void OnGUI () 
    {
        // Show score
        float centerX = Screen.width * 0.5f;
        float centerY = Screen.height * 0.5f;
        Rect offsetRectangle = new Rect(centerX - 80, centerY - 40, 160, 80);
        if (GUI.Button(offsetRectangle, "Final Score \n" + score.ToString("n4")))
        {
            Application.LoadLevel(0);
        }
	}
}
