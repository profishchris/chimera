﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisplayPlayerStats : MonoBehaviour 
{
    public float currentScore = 10.0f;

    private GUIText[] m_PlayerText         = null;
    private EnemySpawner m_EnemySpawner    = null;
    private SquadController m_PlayerSquad  = null;

    void Start()
    {
        m_PlayerText   = GetComponentsInChildren<GUIText>();
        m_PlayerSquad  = FindObjectOfType<SquadController>();
        m_EnemySpawner = FindObjectOfType<EnemySpawner>();
    }

	void Update () 
    {
        // Update player stats
        if (m_PlayerSquad)
        {
            List<PlayerPawn> players = m_PlayerSquad.squadMembers;
            for (int i = 0; i < m_PlayerText.Length; ++i)
            {
                if (i < players.Count)
                {
                    m_PlayerText[i].color = players[i].LightColor;
                    m_PlayerText[i].text = players[i].GetHealth().ToString() + " " + players[i].SpecialAmmmoCount;
                }
                else
                {
                    m_PlayerText[i].text = "";
                }
            }
        }
        
        // Update score info
        if (m_EnemySpawner)
        {
            currentScore = m_EnemySpawner.m_SpawnRate;
            m_PlayerText[4].text = currentScore.ToString("n2");

        }



	}
}
