﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour 
{
    public float fuseTimer = 3.0f;
    public float radius = 3.5f;
    public float damage = 5;

    private float detonationTime;

	// Use this for initialization
	void Start () 
    {
        detonationTime = Time.time + fuseTimer;
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Time.time > detonationTime)
        {
            Collider[] overlappingAgents = Physics.OverlapSphere(transform.position, radius);
            foreach(Collider agent in overlappingAgents)
            {
                if(agent.tag == "Enemy")
                {
                    agent.GetComponent<Enemy>().ModifyHealth(-damage);
                }
            }

            GameObject vfx = (GameObject)Instantiate(Resources.Load("ExplosionEffect"));
            vfx.transform.position = this.transform.position;
            Destroy(this.gameObject);
        }
	}
}
