﻿using UnityEngine;
using System.Collections;

public class AreaHeal : Laser
{
    const int playerMask = 1 << 10;

    // Handle collisions
    protected override void OnTriggerEnter(Collider other)
    {
        // Explode on contact with a pawn, healing all allies in range
        PlayerPawn player = other.GetComponent<PlayerPawn>();
        Enemy enemy = other.GetComponent<Enemy>();
        if (player || enemy)
        {
            HealBurst();
        }

        base.OnTriggerEnter(other);
    }


    private void HealBurst()
    {
        Collider[] effectedPlayers = Physics.OverlapSphere(this.transform.position, 5.0f, playerMask);
        foreach (Collider squadmember in effectedPlayers)
        {
            PlayerPawn player = squadmember.GetComponent<PlayerPawn>();
            player.ModifyHealth(4);
        }
        
        GameObject vfx = (GameObject)Instantiate(Resources.Load("HealBurst"));
        vfx.transform.position = this.transform.position;

        Destroy(this.gameObject);
    }
}
