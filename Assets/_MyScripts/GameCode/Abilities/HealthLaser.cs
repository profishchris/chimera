﻿using UnityEngine;
using System.Collections;

public class HealthLaser : Laser
{
    protected override void OnTriggerEnter(Collider other)
    {
        PlayerPawn ally = other.GetComponent<PlayerPawn>();
        if (ally)
        {
            ally.ModifyHealth(1);
            Destroy(this.gameObject);
        }

        Enemy enemy = other.GetComponent<Enemy>();
        if (enemy)
        {
            enemy.ModifyHealth(-1);
            Destroy(this.gameObject);
        }

        base.OnTriggerEnter(other);
    }
}
