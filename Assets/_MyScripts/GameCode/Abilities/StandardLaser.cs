﻿using UnityEngine;
using System.Collections;

public class StandardLaser : Laser
{
    // Handle collisions
    protected override void OnTriggerEnter(Collider other)
    {
        // Deal damage to the guard
        Enemy guard = other.GetComponent<Enemy>();
        if (guard)
        {
            guard.ModifyHealth(-1);
        }

        base.OnTriggerEnter(other);
    }
}
