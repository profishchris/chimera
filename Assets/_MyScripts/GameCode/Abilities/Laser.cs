﻿using UnityEngine;
using System.Collections;

public abstract class Laser : MonoBehaviour
{
    const int PlayArea = 8;

    public float speed;

    public float lifeSpan;
    private float timeOfDeath;

    // Use this for initialization
    protected virtual void Start()
    {
        GetComponent<Rigidbody>().velocity = transform.forward * speed;
        timeOfDeath = Time.time + lifeSpan;
    }

    void Update()
    {
        // Destory after time
        if (Time.time > timeOfDeath) Destroy(this.gameObject);
    }

     // Handle collisions
    protected virtual void OnTriggerEnter(Collider other)
    {
        // Always collide with play areas
        if (other.gameObject.layer == PlayArea)
        {
            Destroy(this.gameObject);
        }       
    }
}
