﻿using UnityEngine;
using System.Collections;

public class PhysicsVolume : MonoBehaviour 
{
    private Effect currentEffect = Effect.NONE;
    enum Effect { NONE = 0, EARTH, FIRE, WIND };

    private Object vfx = null;
    private Vector3 direction = new Vector3(0, 0, 1.0f);

	// Use this for initialization
	void Start () 
    {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
	}


    void OnTriggerStay(Collider collisionInfo)
    {
        switch (currentEffect)
        {
            case Effect.NONE:
                break;

            case Effect.WIND:
                if(collisionInfo.GetComponent<Rigidbody>()) collisionInfo.GetComponent<Rigidbody>().AddForce(direction * 150.0f);
                break;

            case Effect.FIRE:
                Pawn creature = collisionInfo.GetComponent<Pawn>();
                if (creature)
                {
                    creature.ModifyHealth(-0.025f);
                }
                break;

            case Effect.EARTH:
                Pawn target = collisionInfo.GetComponent<Pawn>();
                if (target)
                {
                    target.currentSpeed = target.speed * 0.35f;
                }
                break;
        }
    }

    public void StartEffect()
    {
        currentEffect = (Effect)Random.Range(0, (int)Effect.WIND + 1);
        Debug.Log(this.name + ": " + currentEffect);

        switch (currentEffect)
        {
            case Effect.NONE:
                vfx = null;
                break;

            case Effect.WIND:
                vfx = Instantiate(Resources.Load("EffectWind"));
                break;

            case Effect.FIRE:
                vfx = Instantiate(Resources.Load("EffectFire"));
                break;

            case Effect.EARTH:
                vfx = Instantiate(Resources.Load("EffectEarth"));
                break;
        }


        if (vfx)
            ((GameObject)vfx).transform.position = this.transform.position;
    }

    public void CancelEffects()
    {
        currentEffect = Effect.NONE;

        if (vfx) Destroy(vfx);
    }
}
