﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySquadController : MonoBehaviour 
{
    public List<Enemy> squadMembers = new List<Enemy>();
    private SquadController playerManager;

	// Use this for initialization
	void Start () 
    {
        playerManager = FindObjectOfType<SquadController>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        // Give enemies new targets if needed
        foreach (Enemy e in squadMembers)
        {
            if (e.target == null)
            {
                e.target = playerManager.squadMembers[Random.Range(0, playerManager.squadMembers.Count)];
            }
        }	    
	}

    // Add to list and assign a target
    public void AddSquadMember(Enemy recruit)
    {
        squadMembers.Add(recruit);

        // Select random target
        PlayerPawn target = playerManager.squadMembers[Random.Range(0, playerManager.squadMembers.Count)];
        recruit.target = target;
    }

    public int SquadSize()
    {
        return squadMembers.Count;
    }
}
