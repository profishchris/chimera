﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SquadController : MonoBehaviour 
{
    private GameController game;
    public List<PlayerPawn> squadMembers = new List<PlayerPawn>();

    public PlayerPawn squadLeader
    {
        get { return squadMembers[0]; }
        set 
        {
            // Squad leader is the first member
            squadMembers.Insert(0, value);
        }
    }


	// Use this for initialization
	void Start () 
    {
        game = FindObjectOfType<GameController>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        int squadCount = squadMembers.Count - 1;
        for (int i = squadCount; i >= 0; i--)
        {
            if (squadMembers[i].GetHealth() < 0.5f)
            {
                Debug.LogError("SquadMemers: " + squadCount + 1 + " HP: " + squadMembers[i].GetHealth());

                if (game.gameStarted)
                {
                    if (i == 0) game.GameOver();
                    DestroyObject(squadMembers[i].gameObject);
                    squadMembers.RemoveAt(i);
                }
            }
        }

	}

    public void AddSquadMember(PlayerPawn recruit)
    {
        squadMembers.Add(recruit);
        recruit.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }

    /// <summary>
    /// Returns the player with the lowest health or
    /// returns the most important injured player
    /// </summary>
    /// <returns> null if all above threashold </returns>
    public PlayerPawn GetHealTarget(float healthThreashold, bool prioritiseRank = false)
    {
        PlayerPawn target = null;
        foreach (PlayerPawn squadie in squadMembers)
        {
            if (squadie.GetHealth() < healthThreashold)
            {
                target = squadie;
                if (prioritiseRank) return target;

                healthThreashold = squadie.GetHealth();
            }
        }

        return target;
    }
}
