using ContextualSteeringBehaviour;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CowardController : AIController
{
    protected Pawn m_Target      = null;

    private bool m_ShowDebug     = true;
    private const int c_Fidelity = 32;
    private int m_UpdateCount    = 50;

    ContextMap m_ContextMap        = new ContextMap(c_Fidelity);
    NavMeshPath m_Path           = new NavMeshPath();

    private bool m_IsPathing;
    private Vector3 m_TargetDestination;


    protected virtual void GenerateContext(bool resetContext = true)
    {
        Vector3 myPosition = transform.position;
        if (resetContext)
        {
            m_ContextMap.ResetContext();
        }

        ////////////////////
        // Evaluate context
        ////////////

        // Enemies pose a danger and we want to move away from them
        List<Enemy> visibleEnemies = GetOtherPawnsInRange<Enemy>(20);
        foreach (Enemy currentPawn in visibleEnemies)
        {
            // Inverse distance
            float mod = 1.0f - ContextAssessor.CalculateNormalizedDistance(currentPawn.transform.position, myPosition, 0.0f, GamePawn.Agent.speed);
            ContextAssessor.SteerAway(m_ContextMap, myPosition, currentPawn.transform.position);
        }


        // Find someone to SteerTowards
        // Avoid obstacles if not pathing, which will take care of avoidance automatically
        Pawn target = GetNearestVisiblePawn<PlayerPawn>(200, true);
        m_TargetDestination = new Vector3();
        if (target == null)
        {
            // Can't see our Ally, so work out a path to them
            m_IsPathing = true;
            target = GetNearestVisiblePawn<PlayerPawn>(200, false);
            NavMesh.CalculatePath(GamePawn.transform.position, target.transform.position, ~GameController.EnviromentMask, m_Path);

            // Find the first corner on the path that isn't too close
            int selectedCorner = 0;
            for (int i = 0; i < m_Path.corners.Length; ++i)
            {
                if (Vector3.Distance(myPosition, m_Path.corners[i]) > 2.5f)
                {
                    selectedCorner = i;
                    break;
                }
            }

            // Steer towards the chosen corner
            if (selectedCorner > -1)
            {
                m_TargetDestination = m_Path.corners[selectedCorner];
                float mod = ContextAssessor.CalculateNormalizedDistance(m_TargetDestination, myPosition, 0.0f, GamePawn.Agent.speed);
                ContextAssessor.SteerTowards(m_ContextMap, myPosition, m_TargetDestination, mod);
            }
        }
        else
        {
            // Can see the Ally, path to them
            m_IsPathing = false;
            m_TargetDestination = target.transform.position;
            float mod = ContextAssessor.CalculateNormalizedDistance(m_TargetDestination, myPosition, 1.0f, GamePawn.Agent.speed * 2.0f);
            ContextAssessor.SteerTowards(m_ContextMap, myPosition, m_TargetDestination, mod);

            // Avoid obstacles
            ContextAssessor.AvoidObstacles(m_ContextMap, myPosition, 1.0f);
        }
    }

    protected virtual void ApplyContext()
    {
        ///////////////////
        // Apply context
        ///////////
        int safestDesired = ContextResolver.FindSafestDesiredSlot(m_ContextMap, 0.5f);
        if (safestDesired > -1)
        {
            SetDestination(m_ContextMap.Directions[safestDesired], m_ContextMap.Positive[safestDesired]);
            return;
        }

        int combinedSafest = ContextResolver.FindCombinedSafestSlot(m_ContextMap);
        if (combinedSafest > -1)
        {
            SetDestination(m_ContextMap.Directions[combinedSafest], m_ContextMap.Positive[combinedSafest]);
        }
    }

    protected virtual void DrawDebug()
    {
        // Draw path
        if (m_IsPathing && m_Path.corners.Length > 0)
        {
            for (int i = 0; i < m_Path.corners.Length - 1; ++i)
            {
                Debug.DrawLine(m_Path.corners[i], m_Path.corners[i + 1]);
            }
        }

        m_ContextMap.DrawDebug(transform.position);

        // Draw the current desired destination
        Debug.DrawLine(transform.position, desiredLocation, Color.yellow);
        Debug.DrawLine(transform.position, m_TargetDestination, Color.magenta);
    }


    /// <summary>
    /// Set a destination projected in the given direction
    /// </summary>
    /// <param name="direction">direction to move in</param>
    /// <param name="speedRatio">percentage of speed at which to project destination</param>
    protected void SetDestination(Vector3 direction, float speedRatio = 1.0f)
    {
        if (GamePawn.Agent.pathPending == false)
        {
            desiredLocation = transform.position + (direction * GamePawn.speed * speedRatio);
            GamePawn.Agent.SetDestination(desiredLocation);
        }

    }

    /// <summary>
    /// Set a destination projected in the given direction
    /// </summary>
    /// <param name="direction">direction to move in</param>
    /// <param name="speedRatio">percentage of speed at which to project destination</param>
    protected void MoveTowardDestination(Vector3 direction, float speedRatio = 1.0f)
    {
        desiredLocation = (direction * GamePawn.speed * speedRatio);
        GamePawn.Agent.speed = GamePawn.speed * speedRatio;
        GamePawn.Agent.Move(direction * speedRatio);
    }

    /// <summary>
    /// Does the controller need a new destination?
    /// </summary>
    /// <returns></returns>
    protected bool NewDestinationRequired()
    {
        m_UpdateCount++;
        if(m_UpdateCount > 6)
        {
            m_UpdateCount = 0;
            return true;
        }

        return false;
    }


    public override void Update()
    {
        base.Update();

        // Attack
        // if (currentTarget = GetNearestVisiblePawn<Enemy>()) UseAbility(currentTarget);
        
        if (NewDestinationRequired())
        {
            GenerateContext();

            ApplyContext();
        }


        if(m_ShowDebug)
        {
            DrawDebug();
        }
    }

}
