﻿using UnityEngine;
using System.Collections;

public class DebugController : GamePadController
{
    public GameObject creature;

    public override void Start()
    {
        base.Start();

        creature = FindObjectOfType<EnemySpawner>().GetPrefab();
    }

    public override void Update()
    {
        base.Update();

        if(Input.GetButtonDown("A"))
        {
           Instantiate(creature, transform.position, transform.rotation);
        }
    }
}
