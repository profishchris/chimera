﻿using UnityEngine;
using System.Collections;

public class MedicController : AIController
{
    public override void Update()
    {
        // Obtain targets
        currentTarget = GetNearestVisiblePawn<Enemy>();
        PlayerPawn healTarget = manager.GetHealTarget(18.0f, true);

        // Preserve self
        DodgeTarget(currentTarget);

        // If not trying ot heal, fight and stay close to leader
        if (!PerfromTriage(healTarget))
        {
            if (currentTarget) UseAbility(currentTarget);
            MaintainProximityToSquadMember(manager.squadLeader);
        }


        base.Update();
    }


    protected virtual bool PerfromTriage(PlayerPawn healTarget)
    {
        // If we have a heal target, try to maintina LoS and fire at them
        if (healTarget)
        {
            MaintainLineOfSightToSquadMember(healTarget);
            GamePawn.transform.LookAt(healTarget.transform.position);

            // If we have special ammo and the player's health is low, use special
            if (GamePawn.SpecialAmmmoCount > 0
            && healTarget.GetHealth() < healTarget.maxHealth * 0.75f)
            {
                ((PlayerClass)GamePawn.CharacterClass).UseSpecialMove(GamePawn.transform, GamePawn.gunBarrel);
            }

            // Use standard move
            ((PlayerClass)GamePawn.CharacterClass).UseStandardMove(GamePawn.transform, GamePawn.gunBarrel);
            return true;
        }

        return false;
    }

    protected override void UseAbility(Pawn target)
    {
        // Face target
        GamePawn.transform.LookAt(target.transform.position);
        ((PlayerClass)GamePawn.CharacterClass).UseStandardMove(GamePawn.transform, GamePawn.gunBarrel);
    }
}
