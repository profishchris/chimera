﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoBehaviour 
{
    const float m_SpawnOffset = 2.0f;

    public static int EnviromentMask = 1 << 8;
    public static int PlayerMask     = 1 << 10;
    public static int EnemyMask      = 1 << 11;

    public GameObject playerPrefab = null;
    public bool gameStarted        = false;
    
    public Pawn Player
    {
        get;
        set;
    }

    public static GameController Instance()
    {
        return m_Instance;
    }

    public void Awake()
    {
        m_Instance = this;

        // Get components
        SquadController squadManager = FindObjectOfType<SquadController>();
        m_Spawner                    = transform.FindChild("PlayerSpawner");

        Player = CreatePlayer<GamePadController, Soldier>(Color.red, GetSpawnPosition());
    }
    

	// Use this for initialization
	public void Start () 
    {
        gameStarted = true;
    }


	
	// Update is called once per frame
	void Update () 
    {
	
	}

    public void GameOver()
    {
        gameStarted = false;

        Object.DontDestroyOnLoad(FindObjectOfType<DisplayPlayerStats>());
        Application.LoadLevel(3);
    }



    private Vector3 GetSpawnPosition()
    {
        if (m_Spawner != null)
            return m_Spawner.position;

        return transform.position;
    }

    private PlayerPawn CreatePlayer<Controller, Job>(Color color, Vector3 position)
        where Controller : PawnController
        where Job : PlayerClass
    {
        GameObject playerObject = (GameObject)Instantiate(playerPrefab);
        PlayerPawn aPlayer = playerObject.GetComponent<PlayerPawn>();

        aPlayer.SetComponents<Controller, Job>();
        aPlayer.transform.position = position;

        aPlayer.TeleportTo(position);

        aPlayer.LightColor = color;

        return aPlayer;
    }




    private static GameController m_Instance = null;
    private Transform m_Spawner              = null;
}
