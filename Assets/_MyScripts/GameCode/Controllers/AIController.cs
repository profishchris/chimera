﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIController : PawnController 
{
    protected const float PlayerProximityCheck = 6.0f;
    protected const float DistanceTolerance    = 2.0f;
    protected const float MinDistance          = 2.0f;

    protected SquadController manager;
    protected Vector3 desiredLocation;

    public Enemy currentTarget;

	// Use this for initialization
	void OnEnable()
    {
        manager = FindObjectOfType<SquadController>();
	}


    public override void Update()
    {
        base.Update();

        // Set animation speed
        GamePawn.AnimSpeed = GamePawn.Agent.velocity.magnitude * GamePawn.speed;
    }

    protected virtual bool DodgeTarget(Enemy target)
    {
        if (target)
        {
            // Maintain distance from target
            Vector3 vectorToTarget = target.transform.position - GamePawn.transform.position;
            float distance         = vectorToTarget.magnitude;
            if (distance < 8.0f)
            {
                vectorToTarget.Normalize();
                GamePawn.Agent.SetDestination(GamePawn.transform.position - (vectorToTarget * GamePawn.Agent.speed));
                //GamePawn.Agent.Move(-vectorToTarget * 0.1f);
                return true;
            }
        }

        return false;
    }
    
    protected virtual void UseAbility(Pawn target)
    {
        // Face target
        GamePawn.transform.LookAt(target.transform.position);

        //// Use special maybe
        //if (GamePawn.CharacterClass.GetSpecialCount() > Random.Range(0, 3))
        //{
        //    if (Random.Range(0, 10) == 0)
        //    {
        //        ((PlayerClass)GamePawn.CharacterClass).UseSpecialMove(GamePawn.transform, GamePawn.gunBarrel);
        //    }
        //}
        // Use standard move
        ((PlayerClass)GamePawn.CharacterClass).UseStandardMove(GamePawn.transform, GamePawn.gunBarrel);
    }



    protected void MaintainProximityToSquadMember(PlayerPawn squadie, float distance = PlayerProximityCheck)
    {
        Vector3 vectorToSquadie = squadie.transform.position - GamePawn.transform.position;
        float distanceFromSquadie = vectorToSquadie.magnitude;
        if (distanceFromSquadie > distance)
        {
            // Pick a location near the leader
            float locationRange = Mathf.Clamp(distance - DistanceTolerance, 2.0f, 5.0f);
            if ((desiredLocation - squadie.transform.position).magnitude > locationRange)
            {
                desiredLocation = GetLocationInRange(-locationRange, locationRange,
                                                     squadie.transform.position);
            }

            // Head to desired location
            if (GamePawn.Agent)
            {
                GamePawn.Agent.SetDestination(desiredLocation);
            }
            else
            {
                Debug.LogError("No Agent");
            }

        }
    }

    protected bool MaintainLineOfSightToSquadMember(PlayerPawn squadie)
    {
        // Check for line of sight
        Vector3 vectorToSquadie = squadie.transform.position - GamePawn.transform.position;
        if (!Physics.Raycast(GamePawn.transform.position, vectorToSquadie, Mathf.Infinity))
        {
            MaintainProximityToSquadMember(squadie, MinDistance);
            return true;
        }

        return false;
    }
}
