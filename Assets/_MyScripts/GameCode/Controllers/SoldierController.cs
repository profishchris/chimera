﻿using UnityEngine;
using System.Collections;

public class SoldierController : AIController 
{

    // Update is called once per frame
    public override void Update()
    {
        // MOve
        currentTarget = SelectTarget();
        if (!DodgeTarget(currentTarget))
        {
            MaintainProximityToSquadMember(manager.squadLeader);
        }

        // Attack
         if (currentTarget) UseAbility(currentTarget);

        base.Update();
    }

    protected virtual Enemy SelectTarget()
    {
        return GetNearestVisiblePawn<Enemy>();
    }
}
