﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeatherController : MonoBehaviour 
{
    public float tick = 10.0f;
    private float nextTickTime;

    private SquadController squadManager;
    private List<PhysicsVolume> cubes = new List<PhysicsVolume>();
    private PhysicsVolume currentCube;

	// Use this for initialization
	void Start () 
    {
        nextTickTime = Time.time + tick;

        squadManager = FindObjectOfType<SquadController>();

        // Add all children volumes
        foreach (PhysicsVolume obj in GetComponentsInChildren<PhysicsVolume>())
        {
            cubes.Add(obj);
        }
        currentCube = cubes[0];
	}
	
	// Update is called once per frame
	void Update () 
    {
        //if (Time.time > nextTickTime)
        //{
        //    // Cancle current
        //    currentCube.CancelEffects();

        //    // Pick the cube closeset to the squad leader
        //    float shortestDistance = 100.0f;
        //    foreach (PhysicsVolume vol in cubes)
        //    {
        //        float distanceToPlayer = (vol.transform.position - squadManager.squadLeader.transform.position).magnitude;
        //        if (distanceToPlayer < shortestDistance)
        //        {
        //            currentCube = vol;
        //            shortestDistance = distanceToPlayer;
        //        }
        //    }

        //    // Start cube;
        //    currentCube.StartEffect();
        //    nextTickTime = Time.time + tick;
        //}
	
	}
}
